#include <napi.h>
#include <node.h>

#include <stdio.h>
#include <string.h>
#include <map>
#include <assert.h>
#include <chrono>
#include <thread>
#include <future>
using namespace std::chrono;

#if defined(_WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#else
#include <sys/select.h>
#include <unistd.h>
#endif

#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>
#include "pocketsphinx.h"

static ps_decoder_t *ps;
static cmd_ln_t *config;
static FILE *rawfd;

/* Sleep for specified msec */
static void
sleep_msec(int32 ms)
{
#if (defined(_WIN32) && !defined(GNUWINCE)) || defined(_WIN32_WCE)
	Sleep(ms);
#else
	/* ------------------- Unix ------------------ */
	struct timeval tmo;

	tmo.tv_sec = 0;
	tmo.tv_usec = ms * 1000;

	select(0, NULL, NULL, NULL, &tmo);
#endif
}

//-------------------------------------------------------------------------------------------------------------------------------
//  Блок с реализацией асинхронной инициализации распознователя голоса  
//-------------------------------------------------------------------------------------------------------------------------------

//настройки аргументов для pocketsphinx
static const arg_t cont_args_def[] = {
	POCKETSPHINX_OPTIONS,
	/* Argument file. */{
		"-argfile",
		ARG_STRING,
		NULL,
		"Argument file giving extra arguments."
	},{
		"-adcdev",
		ARG_STRING,
		NULL,
		"Name of audio device to use for input." },
	{ "-infile",
		ARG_STRING,
		NULL,
		"Audio file to transcribe." },
	{ "-inmic",
		ARG_BOOLEAN,
		"no",
		"Transcribe audio from microphone." },
	{ "-time",
		ARG_BOOLEAN,
		"no",
		"Print word times in file transcription." },
	CMDLN_EMPTY_OPTION
};

// Конфигурация распознователя
struct RecognizerConfig {
	// массив параметров. список минимальных параметров для инициализации: 
	// hmm - путь до гармонической модели 
	// lm - путь до лингвестической модели или 
	// jsgf - путь до файла jsgf
	// dict - путь до словаря
	std::map<std::string, std::string> params;  
	RecognizerConfig() {}
};

// Функция инициализации распознователя голоса. 
// recognizerConfig - конфигурация
// возвращает код rc (0 - в случае успеха)
int RecognizerInitExecute(RecognizerConfig recognizerConfig) {
	char const *cfg;

	//настройка параметров для pocketsphinx
	int argc = recognizerConfig.params.size()*2;	
	char ** argv = new char*[recognizerConfig.params.size() * 2];
	int i = 0;
	for (std::map<std::string, std::string>::iterator it = recognizerConfig.params.begin(); it != recognizerConfig.params.end(); it++) {
		argv[i * 2] = new char[it->first.size() + 1];
		strcpy(argv[i * 2], it->first.c_str());
		argv[i * 2 + 1] = new char[it->second.size() + 1];
		strcpy(argv[i * 2 + 1], it->second.c_str());
		i++;
	}

	for (int i = 0; i < (int)(argc/2); i++) {
		E_INFO(argv[i*2]);
		E_INFO(": ");
		E_INFO(argv[i * 2 + 1]);
		E_INFO("\r\n");
	}

	config = cmd_ln_parse_r(NULL, cont_args_def, argc, argv, TRUE);

	if (config && (cfg = cmd_ln_str_r(config, "-argfile")) != NULL) {
		config = cmd_ln_parse_file_r(config, cont_args_def, cfg, FALSE);
	}

	if (config == NULL || (cmd_ln_str_r(config, "-infile") == NULL && cmd_ln_boolean_r(config, "-inmic") == FALSE)) {
		E_INFO("Specify '-infile <file.wav>' to recognize from file or '-inmic yes' to recognize from microphone.\n");
		cmd_ln_free_r(config);		
		for (size_t i = 0; i < argc; i++) delete[] argv[i];
		delete[] argv;
		return 1;
	}

	//инициализация pocketsphinx
	ps_default_search_args(config);
	ps = ps_init(config);
	if (ps == NULL) {
		cmd_ln_free_r(config);
		for (size_t i = 0; i < argc; i++) delete[] argv[i];
		delete[] argv;
		return 2;
	}

	for (size_t i = 0; i < argc; i++) delete[] argv[i];
	delete[] argv;

	E_INFO("COMPILED ON: %s, AT: %s\n\n", __DATE__, __TIME__);

	//recognize_from_microphone();

	//ps_free(ps);
	//cmd_ln_free_r(config);

	return 0;
}

// Класс для асинхронной инициализации распознователя
class RecognizerInitAsyncWorker : public Napi::AsyncWorker {
private:
	RecognizerConfig recognizerConfig;
	int result_code = 0;
public:
	RecognizerInitAsyncWorker(const Napi::Function& callback, RecognizerConfig recognizerConfig) : Napi::AsyncWorker(callback), recognizerConfig(recognizerConfig) { printf("RecognizerInitAsyncWorker: Constructor\n"); }
protected:
	// то, что будет выполняться асинхронно
	void Execute() override {
		try {
			printf("RecognizerInitAsyncWorker: Execute\n");
			result_code = RecognizerInitExecute(recognizerConfig);
			printf("RecognizerInitAsyncWorker: Execute End (rc = %i)\n", result_code);
		}
		catch (int ex) {
			printf("RecognizerInitAsyncWorker: Error in Execute: ex=%i\n", ex);
		}
	}

	// выполнится в случае успешного завершения 
	void OnOK() override {
		try {
			printf("RecognizerInitAsyncWorker: OnOK\n");
			Napi::Env env = Env();
			Napi::Object result = Napi::Object::New(env);
			result.Set("rc", result_code);
			Callback().MakeCallback(Receiver().Value(), { env.Null(), result });
		}
		catch (int ex) {
			printf("RecognizerInitAsyncWorker: Error in OnOK: ex=%i\n", ex);
		}
	}

	// выполнится в случае ошибки
	void OnError(const Napi::Error& e) override {
		try {
			printf("RecognizerInitAsyncWorker: OnError\n");
			Napi::Env env = Env();
			Callback().MakeCallback(Receiver().Value(), { e.Value(), env.Undefined() });
		}
		catch (int ex) {
			printf("RecognizerInitAsyncWorker: Error in OnError: ex=%i\n", ex);
		}
	}
};

//функция запуска асинхронной инициализации распознователя
void RecognizerInit(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	try {
		RecognizerConfig recognizerConfig;

		if (info.Length() < 2) {
			Napi::TypeError::New(env, "Invalid arguments count. First - config object; Second - callback function.").ThrowAsJavaScriptException();
			return;
		}
		if (!info[0].IsObject() || !info[1].IsFunction()) {
			Napi::TypeError::New(env, "Invalid argument types. First - config object; Second - callback function.").ThrowAsJavaScriptException();
			return;
		}

		Napi::Function callback = info[1].As<Napi::Function>();
		Napi::Object params = info[0].ToObject();

		Napi::Array params_keys = params.GetPropertyNames();
		std::vector<std::string> params_array;
		bool is_inmic = false;
		for (int i = 0; i < params_keys.Length(); i++) {
			std::string key = "-" + params_keys.Get(i).As<Napi::String>().Utf8Value();
			recognizerConfig.params.insert(std::pair<std::string, std::string>(key, params.Get(params_keys.Get(i)).As<Napi::String>().Utf8Value()));
			if (key == "inmic") is_inmic = true;
		}
		if(!is_inmic) recognizerConfig.params.insert(std::pair<std::string, std::string>("-inmic", "yes"));
		
		(new RecognizerInitAsyncWorker(callback, recognizerConfig))->Queue();
	}
	catch (int ex) {
		std::string message = std::string("Error in RecognizerInit: ex=") + std::to_string(ex);
		Napi::TypeError::New(env, message).ThrowAsJavaScriptException();
	}

}

//-------------------------------------------------------------------------------------------------------------------------------
//  Блок с реализацией асинхронной инициализации распознователя голоса  
//-------------------------------------------------------------------------------------------------------------------------------

//текущий статус распознования 
struct RecognizerStatus {
	int circleCount = 0;		//цикл прохода  
	bool inSpeech = false;	//слышит ли микрофон голос в данный момент
	bool startedHypAnalyse = false; //начато ли анализирование гипотиз по записи
	int timeInterval = 0;	//время, прошедшее со старта распознования (в миллисекундах)  	
};

//спсиок параметров для функиции распознования
struct RecognizerStartParams {
	//событие на окончанию распознования
	bool isOnRecognize = false;
	Napi::Function onRecognize;
	//событие при начале определения голоса в микрофоне
	bool isOnStartSpeech = false;
	Napi::Function onStartSpeech;
	//событие на начало определения голоса в микрофоне
	bool isOnEndSpeech = false;
	Napi::Function onEndSpeech;
	//событие на начало анализирования гипотиз
	bool isOnStartHypAnalyse = false;
	Napi::Function onStartHypAnalyse;
};

//текущий статус распознования 
RecognizerStatus recognizerStatus;
high_resolution_clock::time_point recognizerStartTime = high_resolution_clock::now(); //время от начала распознования
bool needRecognizerStop = false;

Napi::Function test;

// Функция запускает процесс распознования. Возвращает статус (0 в случае успеха) и текст в result
int RecognizerStartExecute(std::string& result) {
	ad_rec_t *ad;
	int16 adbuf[2048];
	uint8 utt_started, in_speech;
	int32 k;
	char const *hyp;

	result = "";
	if (needRecognizerStop) return 0;	

	if ((ad = ad_open_dev(cmd_ln_str_r(config, "-adcdev"),
		(int)cmd_ln_float32_r(config,
			"-samprate"))) == NULL) {
		result = "Failed to open audio device";
		return 1;
	}

	if (needRecognizerStop) return 0;

	if (ad_start_rec(ad) < 0) {
		result = "Failed to start recording";
		return 2;
	}

	if (needRecognizerStop) return 0;

	if (ps_start_utt(ps) < 0) {
		result = "Failed to start utterance";
		return 3;
	}

	if (needRecognizerStop) return 0;

	utt_started = FALSE;
	E_INFO("Ready....\n");

	for (;;) {
		if ((k = ad_read(ad, adbuf, 2048)) < 0) {
			result = "Failed to read audio";
			return 4;
		}

		ps_process_raw(ps, adbuf, k, FALSE, FALSE);
		in_speech = ps_get_in_speech(ps);
		printf("in_speech = %i\n", in_speech);
		recognizerStatus.inSpeech = in_speech != 0;
		if (in_speech && !utt_started) {
			utt_started = TRUE;
			E_INFO("Listening...\n");
		}
		if ((!in_speech && utt_started) || needRecognizerStop) {
			recognizerStatus.startedHypAnalyse = true;

			E_INFO("before ps_end_utt\n");
			/* speech -> silence transition, time to start new utterance  */
			ps_end_utt(ps);
			E_INFO("before ps_get_hyp\n");
			hyp = ps_get_hyp(ps, NULL);
			E_INFO("after ps_get_hyp\n");
			if (hyp != NULL) {
				printf("%s\n", hyp);
				fflush(stdout);

				ad_close(ad);
				result = hyp;
				return 0;
			} else if(needRecognizerStop) return 0;

			if (ps_start_utt(ps) < 0) {
				result = "Failed to start utterance";
				return 5;
			}

			utt_started = FALSE;
			E_INFO("Ready....\n");
		}
		recognizerStatus.circleCount++;
		
		sleep_msec(100);
	}
	ad_close(ad);

	result = "Unknown result";
	return 6;
}

static void print_word_times()
{
	int frame_rate = cmd_ln_int32_r(config, "-frate");
	ps_seg_t *iter = ps_seg_iter(ps);
	while (iter != NULL) {
		int32 sf, ef, pprob;
		float conf;

		ps_seg_frames(iter, &sf, &ef);
		pprob = ps_seg_prob(iter, NULL, NULL, NULL);
		conf = logmath_exp(ps_get_logmath(ps), pprob);
		printf("%s %.3f %.3f %f\n", ps_seg_word(iter), ((float)sf / frame_rate),
			((float)ef / frame_rate), conf);
		iter = ps_seg_next(iter);
	}
}

static int CheckWavHeader(char *header, int expected_sr)
{
	int sr;

	if (header[34] != 0x10) {
		E_ERROR("Input audio file has [%d] bits per sample instead of 16\n", header[34]);
		return 0;
	}
	if (header[20] != 0x1) {
		E_ERROR("Input audio file has compression [%d] and not required PCM\n", header[20]);
		return 0;
	}
	if (header[22] != 0x1) {
		E_ERROR("Input audio file has [%d] channels, expected single channel mono\n", header[22]);
		return 0;
	}
	sr = ((header[24] & 0xFF) | ((header[25] & 0xFF) << 8) | ((header[26] & 0xFF) << 16) | ((header[27] & 0xFF) << 24));
	if (sr != expected_sr) {
		E_ERROR("Input audio file has sample rate [%d], but decoder expects [%d]\n", sr, expected_sr);
		return 0;
	}
	return 1;
}

/*
* Continuous recognition from a file
*/
std::string RecognizeFromFileFunc(const char *fname){
	int16 adbuf[2048];
	const char *hyp;
	int32 k;
	uint8 utt_started, in_speech;
	int32 print_times = cmd_ln_boolean_r(config, "-time");

	//fname = cmd_ln_str_r(config, "-infile");
	if ((rawfd = fopen(fname, "rb")) == NULL) {
		E_FATAL_SYSTEM("Failed to open file '%s' for reading",fname);
		return "";
	}

	if (strlen(fname) > 4 && strcmp(fname + strlen(fname) - 4, ".wav") == 0) {
		char waveheader[44];
		fread(waveheader, 1, 44, rawfd);
		if (!CheckWavHeader(waveheader, (int)cmd_ln_float32_r(config, "-samprate"))) {
			E_FATAL("Failed to process file '%s' due to format mismatch.\n", fname);
			return "";
		}	
	}

	if (strlen(fname) > 4 && strcmp(fname + strlen(fname) - 4, ".mp3") == 0) {
		E_FATAL("Can not decode mp3 files, convert input file to WAV 16kHz 16-bit mono before decoding.\n");
		return "";
	}

	ps_start_utt(ps);
	utt_started = FALSE;

	while ((k = fread(adbuf, sizeof(int16), 2048, rawfd)) > 0) {
		ps_process_raw(ps, adbuf, k, FALSE, FALSE);
		in_speech = ps_get_in_speech(ps);
		if (in_speech && !utt_started) {
			utt_started = TRUE;
		}
		if (!in_speech && utt_started) {
			ps_end_utt(ps);
			hyp = ps_get_hyp(ps, NULL);
			if (hyp != NULL)
				printf("%s\n", hyp);
			if (print_times)
				print_word_times();
			fflush(stdout);

			ps_start_utt(ps);
			utt_started = FALSE;
		}
	}
	ps_end_utt(ps);
	if (utt_started) {
		hyp = ps_get_hyp(ps, NULL);
		if (hyp != NULL) {
			printf("%s\n", hyp);
			if (print_times) {
				print_word_times();
			}
		}
	}

	fclose(rawfd);
	std::string result(hyp);
	return result;
}

Napi::String RecognizeFromFile(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	try {
		recognizerStatus.circleCount = 0;
		recognizerStatus.inSpeech = false;
		recognizerStatus.startedHypAnalyse = false;
		recognizerStatus.timeInterval = 0;
		needRecognizerStop = false;

		if (info.Length() < 2) {
			Napi::TypeError::New(env, "Invalid arguments count. First - file_name;second - callback function.").ThrowAsJavaScriptException();
			return Napi::String::New(env, "");
		}

		RecognizerStartParams recognizerStartParams;

		if (!info[0].IsString() || !info[1].IsFunction()) {
			Napi::TypeError::New(env, "Invalid arguments count. First - file_name;second - callback function.").ThrowAsJavaScriptException();
			return Napi::String::New(env, "");
		}

		Napi::Function callback = info[1].As<Napi::Function>();
		std::string file_name = info[0].As<Napi::String>().Utf8Value();

		return Napi::String::New(env, RecognizeFromFileFunc(file_name.c_str()));
	}
	catch (int ex) {
		printf("RecognizeFromFile: Error in OnError: ex=%i\n", ex);
	}
}


// Класс для запуска асинхронного распознования
class RecognizerStartAsyncWorker : public Napi::AsyncWorker {
private:
	int result_code = 0;
	std::string result_text;
	RecognizerStartParams recognizerStartParams;
	bool recognizeEnded = false;
public:
	RecognizerStartAsyncWorker(RecognizerStartParams _recognizerStartParams): 
		Napi::AsyncWorker(_recognizerStartParams.onRecognize), 
		recognizerStartParams(_recognizerStartParams)
	{
		printf("RecognizerStartAsyncWorker: Constructor\n"); 
	}
protected:
	// то, что будет выполняться асинхронно
	void Execute() override {
		try {
			recognizeEnded = false;
			printf("RecognizerStartAsyncWorker: Execute\n");
			result_code = RecognizerStartExecute(result_text);
			printf("RecognizerStartAsyncWorker: Execute End (rc = %i)\n", result_code);
		}
		catch (int ex) {
			printf("RecognizerStartAsyncWorker: Error in Execute: ex=%i\n", ex);
		}
	}

	// выполнится в случае успешного завершения 
	void OnOK() override {
		try {
			recognizeEnded = true;
			printf("RecognizerStartAsyncWorker: OnOK\n");
			Napi::Env env = Env();
			Napi::Object result = Napi::Object::New(env);
			result.Set("rc", result_code);
			result.Set("result", result_text);
			if(recognizerStartParams.isOnRecognize) Callback().MakeCallback(Receiver().Value(), { env.Null(), result });
		}
		catch (int ex) {
			printf("RecognizerStartAsyncWorker: Error in OnOK: ex=%i\n", ex);
		}
	}

	// выполнится в случае ошибки
	void OnError(const Napi::Error& e) override {
		try {
			recognizeEnded = true;
			printf("RecognizerStartAsyncWorker: OnError\n");
			Napi::Env env = Env();
			Callback().MakeCallback(Receiver().Value(), { e.Value(), env.Undefined() });
		}
		catch (int ex) {
			printf("RecognizerStartAsyncWorker: Error in OnError: ex=%i\n", ex);
		}
	}	
};


//функция запуска асинхронного распознования голоса
void RecognizerStart(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	try {
		RecognizerConfig recognizerConfig;

		recognizerStartTime = high_resolution_clock::now();
		recognizerStatus.circleCount = 0;
		recognizerStatus.inSpeech = false;
		recognizerStatus.startedHypAnalyse = false;
		recognizerStatus.timeInterval = 0;
		needRecognizerStop = false;
		
		if (info.Length() < 1) {
			Napi::TypeError::New(env, "Invalid arguments count. First - callback function.").ThrowAsJavaScriptException();
			return;
		}
		
		RecognizerStartParams recognizerStartParams;

		if (info[0].IsFunction()) {
			recognizerStartParams.onRecognize = info[0].As<Napi::Function>();
			recognizerStartParams.isOnRecognize = true;
		}
		else if (info[0].IsObject()) {
			Napi::Object params = info[0].As<Napi::Object>();
			Napi::Array params_keys = params.GetPropertyNames();
			std::vector<std::string> params_array;
			for (int i = 0; i < params_keys.Length(); i++) {
				std::string key = params_keys.Get(i).As<Napi::String>().Utf8Value();
				if (key == "onRecognize") {
					recognizerStartParams.onRecognize = params.Get(params_keys.Get(i)).As<Napi::Function>();
					recognizerStartParams.isOnRecognize = true;
				} else if (key == "onStartSpeech") {
					recognizerStartParams.onStartSpeech = params.Get(params_keys.Get(i)).As<Napi::Function>();
					recognizerStartParams.isOnStartSpeech = true;

					//test = recognizerStartParams.onStartSpeech;

				} if (key == "onEndSpeech") {
					recognizerStartParams.onEndSpeech = params.Get(params_keys.Get(i)).As<Napi::Function>();
					recognizerStartParams.isOnEndSpeech = true;
				} if (key == "onStartHypAnalyse") {
					recognizerStartParams.onStartHypAnalyse = params.Get(params_keys.Get(i)).As<Napi::Function>();
					recognizerStartParams.isOnEndSpeech = true;
				}
			}
		}
		else {
			Napi::TypeError::New(env, "Invalid argument types. First - callback function.").ThrowAsJavaScriptException();
			return;
		}

		(new RecognizerStartAsyncWorker(recognizerStartParams))->Queue();
	}
	catch (int ex) {
		std::string message = std::string("Error in RecognizerStart: ex=") + std::to_string(ex);
		Napi::TypeError::New(env, message).ThrowAsJavaScriptException();
	}
}

//функция для остановки процесса распознования
void RecognizerStop(const Napi::CallbackInfo& info) {
	needRecognizerStop = true;
}

//функция для получения статуса распознования
Napi::Object getRecognizerStatus(const Napi::CallbackInfo& info) {
	high_resolution_clock::time_point currentTime = high_resolution_clock::now();
	recognizerStatus.timeInterval = duration_cast<milliseconds>(currentTime - recognizerStartTime).count();

	Napi::Env env = info.Env();
	Napi::Object result = Napi::Object::New(env);
	result.Set("cicleCount", recognizerStatus.circleCount);
	result.Set("inSpeech", recognizerStatus.inSpeech);
	result.Set("startedHypAnalyse", recognizerStatus.startedHypAnalyse);
	result.Set("timeInterval", recognizerStatus.timeInterval);
	return result;
}

//-------------------------------------------------------------------------------------------------------------------------------
//  Блок инициализации модуля
//-------------------------------------------------------------------------------------------------------------------------------

Napi::Object init(Napi::Env env, Napi::Object exports) {
	exports.Set(Napi::String::New(env, "recognizerInit"), Napi::Function::New(env, RecognizerInit));
	exports.Set(Napi::String::New(env, "recognizerStart"), Napi::Function::New(env, RecognizerStart));
	exports.Set(Napi::String::New(env, "recognizerStop"), Napi::Function::New(env, RecognizerStop));
	exports.Set(Napi::String::New(env, "recognizeFromFile"), Napi::Function::New(env, RecognizeFromFile));
	exports.Set(Napi::String::New(env, "getRecognizerStatus"), Napi::Function::New(env, getRecognizerStatus));
	return exports;
};

NODE_API_MODULE(sphinx, init);
