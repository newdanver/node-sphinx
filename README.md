node-sphinx: модуль node.js для работы с распознаванием голоса
===============================================================================

Модуль работает через пакет распазнования голоса CMUSphinx (https://github.com/cmusphinx).

Установка под Windows.
-------------------------------------------------------------------------------

Для установки требуется, чтобы бы в системе были установлены:
 - [Node.js](https://nodejs.org/en/download/)
 - [git](https://git-scm.com/downloads)
 - windows-build-tools (npm install --global --production windows-build-tools)
 - [Распространяемый пакет Visual C++ для Visual Studio 2012](https://www.microsoft.com/ru-ru/download/details.aspx?id=30679)
 - [Распространяемый пакет Visual C++ для Visual Studio 2015](https://www.microsoft.com/ru-ru/download/details.aspx?id=48145)
 

В корне своего проекта NodeJS выполните команду: 

	npm i node-sphinx
	
Если требуется пересборка модуля, перейдите в папку модуля (node_modules/node-sphinx) и выполните команду:

	npm run install
	
Замечание: сборка модуля происходит автоматически при его установке	
	
	
Установка под Linux.
-------------------------------------------------------------------------------

Для установки требуется, чтобы бы в системе были установлены:
 - Node.js (sudo apt-get install nodejs)
 - npm (sudo apt-get install npm)
 - git (sudo apt-get install git)

В корне своего проекта NodeJS выполните команду: 

	npm i node-sphinx --save
	
Замечание: при установке может быть запрошен пароль sudo, так происходит установка дополнительных компонентов в систему таких,
как аудио драйверы, средства для сборки проектов и библиотеки пакетов pocketsphinx и sphinxbase. Список устанавлеваемых пакетов: 
build-essential, autoconf, libtool, automake, bison, python2.7, python2.7-dev, swig, dos2unix, pavucontrol, linuxsoundbase, 
alsa-base, alsa-utils, oss-compat, libsound2, pulseaudio, libpulse-dev, osspd.  
	
Если требуется пересборка модуля, перейдите в папку модуля (node_modules/node-sphinx) и выполните команду:

	npm run install
	
Замечание: сборка модуля происходит автоматически при его установке


Как пользоваться.		
-------------------------------------------------------------------------------

Подключите модуль:

	const sphinx = require("node-sphinx");

Инициализируете распознаватель: 

	sphinx.recognizerInit(params,callback) 
						
params - объект с параметрами для инициализации. Спсиок всех параметров можно посмотреть [здесь](https://github.com/cmusphinx/pocketsphinx/blob/master/doc/pocketsphinx_continuous.1). 
Основные параметры, требуемые для инициализации:
 - hmm: путь до акустической модели;
 - lm: путь до лингвистической модели (или jsgf: путь до файла jsgf); 
 - dict: путь до словаря.
 
callback - function(error,data): функция, которая будет вызвана по завершению инициализации. В случае
успешной инициализации error будет равен null, data.rc будет равен 0.
Пример:

	sphinx.recognizerInit({
		hmm: path.join(__dirname,"node_modules/node-sphinx/src/pocketsphinx/model/en-us/en-us"),
		lm: path.join(__dirname,"node_modules/node-sphinx/src/pocketsphinx/model/en-us/en-us.lm.bin"),
		dict: path.join(__dirname,"node_modules/node-sphinx/src/pocketsphinx/model/en-us/cmudict-en-us.dict")
    },(error,data) => {
        if(error) {
            console.log(error);            
            return;            
        }
        if(data.rc != 0) {
            console.log("Ошибка инициализации: rc=" + data.rc);
            return;
        }
		
		console.log("Инициализированно");        
    });

Запустите распознавание:

	sphinx.recognizerStart(callback)	

callback - function(error,data): функция, которая будет вызвана по завершению распознавания. В случае
успешной инициализации error будет равен null, data.rc будет равен 0, а в data.result будет лежать распознанная строка.
Пример:

	sphinx.recognizerStart((error,data) => {
        if(error) {
            console.log(error);
            return;
        }
        if(data.rc != 0) {
            console.log("Ошибка распознавания: rc=" + data.rc + "; message=" + data.result);
            return;
        }

        console.log("Распознано: " + data.result);            
    });
	
Остановка распознования: 
 	
	sphinx.recognizerStop()
	
Остановка распознования происходит автоматически после того, как система услышит голос и распознает его. Но процесс
распознования можно прервать данной функцией. 

Получение статуса распознования: 

	sphinx.getRecognizerStatus()
	
Данная функция возвращает текущее состояние распознования. Результатом является объект следующего вида:

	{
		inSpeech,			//слышит ли микрофон голос в данный момент
		startedHypAnalyse, 	//начато ли анализирование гипотиз по записи
		timeInterval		//время, прошедшее со старта распознования  
	}

Пример:

    setInterval(() => {
		let status = sphinx.getRecognizerStatus();
        console.log(status.inSpeech + "; " + status.startedHypAnalyse + "; " + status.timeInterval);
    },100);				

		

