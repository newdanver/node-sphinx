@echo off
Setlocal Enabledelayedexpansion Enableextensions

Set From=v110
Set Into=v140
Set Filename=%1
Set NewFileName=file.txt
 
if exist "%NewFileName%" del "%NewFileName%"
for /F "UseBackQ delims= eol=" %%A in ("%filename%") do (
  Set St=%%A
  Set St=!ST:%From%=%Into%!
  Echo !St!>>"%NewFileName%"
)

copy /y %NewFileName% %Filename% >nul
del /f /q %NewFileName% >nul
