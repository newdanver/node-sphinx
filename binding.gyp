{
	"targets": [
		{ 
			"cflags!": [ "-fno-exceptions" ],
			"cflags_cc!": [ "-fno-exceptions" ],
			"include_dirs" : [
				"src/pocketsphinx/include",
				"src/sphinxbase/include",
				"src/sphinxbase/include/win32",
				"<!@(node -p \"require('node-addon-api').include\")"
			],
			"conditions": [
				[ "OS=='linux'", {
					"ldflags": ["-Wl,-rpath=/usr/local/lib"],
					"libraries":[
						"/usr/local/lib/libsphinxbase.so",
						"/usr/local/lib/libpocketsphinx.so",
						"/usr/local/lib/libsphinxad.so"
					],
	
				}],
				[ "OS=='win'", {
					"libraries": [
						"<(module_root_dir)/distr/sphinxbase.lib",
						"<(module_root_dir)/distr/pocketsphinx.lib",
					],
				}],
			],
			"target_name": "sphinx",
			"sources": [ "src/sphinx.cc" ],
			'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
		}
	]
}

