cd src	
git clone https://github.com/cmusphinx/sphinxbase.git
cd sphinxbase
git checkout .
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_cepview.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_fe.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_jsgf2fsg.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_lm_convert.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_pitch.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinx_seg.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\sphinxbase.vcxproj
@rem C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe sphinxbase.sln /p:Configuration=Release /p:Platform=x64

cd ..
git clone https://github.com/cmusphinx/pocketsphinx.git
cd pocketsphinx
git checkout .
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\pocketsphinx.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\pocketsphinx_batch.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\pocketsphinx_continuous.vcxproj
@rem call ..\..\change_platform_ver.cmd win32\sphinx_cepview\pocketsphinx_mdef_convert.vcxproj
@rem C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe pocketsphinx.sln /p:Configuration=Release /p:Platform=x64

cd ../..
node-gyp rebuild

