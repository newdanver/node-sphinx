#!/bin/bash
sudo apt-get update
yes | sudo apt-get install nodejs
yes | sudo apt-get install npm 
yes | sudo apt-get install build-essential
yes | sudo apt-get install autoconf
yes | sudo apt-get install libtool
yes | sudo apt-get install automake
yes | sudo apt-get install bison
yes | sudo apt-get install python2.7
yes | sudo apt-get install python2.7-dev
yes | sudo apt-get install swig
yes | sudo apt-get install git
yes | sudo apt-get install dos2unix

yes | sudo apt-get install pavucontrol
yes | sudo apt-get install linuxsoundbase
yes | sudo apt-get install alsa-base
yes | sudo apt-get install alsa-utils
yes | sudo apt-get install oss-compat
yes | sudo apt-get install libsound2
yes | sudo apt-get install pulseaudio
yes | sudo apt-get install libpulse-dev
yes | sudo apt-get install osspd

cd src
sudo rmdir -R sphinxbase 
git clone https://github.com/cmusphinx/sphinxbase.git
sudo chmod 777 -R sphinxbase
cd sphinxbase
find . -name \*.m4|xargs dos2unix
find . -name \*.ac|xargs dos2unix
find . -name \*.am|xargs dos2unix
sudo chmod +x autogen.sh
dos2unix autogen.sh
./autogen.sh	
./autogen.sh
./configure
make
sudo make install

cd ..
sudo rmdir -R pocketsphinx
git clone https://github.com/cmusphinx/pocketsphinx.git
sudo chmod 777 -R pocketsphinx
cd pocketsphinx
find . -name \*.m4|xargs dos2unix
find . -name \*.ac|xargs dos2unix
find . -name \*.am|xargs dos2unix
sudo chmod +x autogen.sh
dos2unix autogen.sh
./autogen.sh
./autogen.sh
./configure
make
sudo make install





